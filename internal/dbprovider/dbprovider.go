package dbprovider

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // postgres driver
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/settings"
)

const (
	connLifeTimeMin = 5
	maxCons         = 5
)

func NewConn() (*sqlx.DB, error) {
	database, err := sqlx.Connect("postgres", connStr())
	if err != nil {
		return nil, err
	}
	database.SetMaxOpenConns(maxCons)
	database.SetConnMaxIdleTime(time.Minute * time.Duration(connLifeTimeMin))
	database.SetConnMaxLifetime(time.Minute * time.Duration(connLifeTimeMin))

	return database, nil
}

func connStr() string {
	settings := settings.AppSettings

	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
		settings.DBHost, settings.DBLogin, settings.DBPassword, settings.DBName)
}
