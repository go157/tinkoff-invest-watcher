package selfapi_test

import (
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/marketdata"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/mock_api"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/selfapi"
)

type test struct {
	name         string
	app          marketdata.MarketApp
	ginContext   ginContext
	codeResponse int
	bodyResponse string
}

type ginContext struct {
	input   *gin.Context
	recoder *httptest.ResponseRecorder
}

type brokenReader struct{}

var someErr = errors.New("some error")

func TestHandlerInstrumentsPut(t *testing.T) {
	t.Parallel()

	tests := []test{
		{
			name: "API: valid instrument level",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().TikerExist("SBER").Return(true)
				m.EXPECT().SetLevelByTiker("SBER", int8(10)).Return(nil)
				m.EXPECT().SendMessage("Инструменту SBER установлен уровень 10")
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER","trigger_level":10}`, nil),
			codeResponse: 200,
		},
		{
			name: "API: valid instrument active",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().TikerExist("SBER").Return(true)
				m.EXPECT().SetActiveByTiker("SBER", true).Return(nil)
				m.EXPECT().SendMessage("Инструменту SBER установлена активность true")
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER","active":true}`, nil),
			codeResponse: 200,
		},
		{
			name: "API: msg error instrument level",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().TikerExist("SBER").Return(true)
				m.EXPECT().SetLevelByTiker("SBER", int8(10)).Return(nil)
				m.EXPECT().SendMessage("Инструменту SBER установлен уровень 10").Return(someErr)
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER","trigger_level":10}`, nil),
			codeResponse: 400,
		},
		{
			name: "API: msg error instrument active",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().TikerExist("SBER").Return(true)
				m.EXPECT().SetActiveByTiker("SBER", true).Return(nil)
				m.EXPECT().SendMessage("Инструменту SBER установлена активность true").Return(someErr)
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER","active":true}`, nil),
			codeResponse: 400,
		},
		{
			name: "API: invalid json on instrument put",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				return m
			}(),
			ginContext:   createInput(`fff`, nil),
			codeResponse: 400,
		},
		{
			name: "API: invalid json on instrument put - wrong level",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER","trigger_level":true}`, nil),
			codeResponse: 400,
		},
		{
			name: "API: invalid json on instrument put - wrong active",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER","active":6}`, nil),
			codeResponse: 400,
		},
		{
			name: "API: invalid json on instrument put - no tiker",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				return m
			}(),
			ginContext:   createInput(`{"active":true}`, nil),
			codeResponse: 400,
		},
		{
			name: "API: empty tiker on instrument put",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				return m
			}(),
			ginContext:   createInput(`{"tiker":"","active":true}`, nil),
			codeResponse: 400,
		},
		{
			name: "API: unknown tiker on instrument put",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().TikerExist("SBER1").Return(false)
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER1","active":true}`, nil),
			codeResponse: 400,
		},
		{
			name: "API: error on instrument active",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().TikerExist("SBER").Return(true)
				m.EXPECT().SetActiveByTiker("SBER", true).Return(errors.New("some error"))
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER","active":true}`, nil),
			codeResponse: 400,
		},
		{
			name: "API: error on instrument trigger level",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().TikerExist("SBER").Return(true)
				m.EXPECT().SetLevelByTiker("SBER", int8(10)).Return(errors.New("some error"))
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER","trigger_level":10}`, nil),
			codeResponse: 400,
		},
		{
			name: "API: wrong input",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				return m
			}(),
			ginContext:   createWrongInput(nil),
			codeResponse: 400,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			handler := selfapi.NewHandler(test.app)
			handler.HandlerInstrumentsPut(test.ginContext.input)
			require.Equal(t, test.codeResponse, test.ginContext.recoder.Code)
		})
	}
}

func TestHandlerInstrumentsGet(t *testing.T) {
	t.Parallel()

	tests := []test{
		{
			name: "API: valid instrument get",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().SendTikerInfo("SBER").Return(nil)
				return m
			}(),
			ginContext:   createInput("", map[string]string{"tiker": "SBER"}),
			codeResponse: 200,
		},
		{
			name: "API: error on instrument get",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().SendTikerInfo("SBER").Return(errors.New("some error"))
				return m
			}(),
			ginContext:   createInput("", map[string]string{"tiker": "SBER"}),
			codeResponse: 400,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			handler := selfapi.NewHandler(test.app)
			handler.HandlerInstrumentsGet(test.ginContext.input)
			require.Equal(t, test.codeResponse, test.ginContext.recoder.Code)
		})
	}
}

func TestHandlerEventsGet(t *testing.T) {
	t.Parallel()

	tests := []test{
		{
			name: "API: valid event get",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().GetNewEvents().Return([]string{"foo", "bar"}, nil)
				return m
			}(),
			ginContext:   createInput("", nil),
			codeResponse: 200,
			bodyResponse: `[{"Type":"message","Message":"foo"},{"Type":"message","Message":"bar"}]`,
		},
		{
			name: "API: on event get error",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().GetNewEvents().Return([]string{"foo", "bar"}, errors.New("some error"))
				return m
			}(),
			ginContext:   createInput("", nil),
			codeResponse: 400,
			bodyResponse: `{"message":"some error"}`,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			handler := selfapi.NewHandler(test.app)
			handler.HandlerEventsGet(test.ginContext.input)
			body, err := io.ReadAll(test.ginContext.recoder.Body)
			require.Nil(t, err)
			require.Equal(t, test.codeResponse, test.ginContext.recoder.Code)
			require.Equal(t, test.bodyResponse, string(body))
		})
	}
}

func TestHandlerBellsPost(t *testing.T) {
	t.Parallel()

	tests := []test{
		{
			name: "API: valid bell set",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().
					SetBell(&marketdata.Instrument{
						Tiker: "SBER",
					}, 120.23).
					Return(nil)
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER","target":120.23}`, nil),
			codeResponse: 200,
		},
		{
			name: "API: error on bell set",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				m.EXPECT().
					SetBell(&marketdata.Instrument{
						Tiker: "SBER",
					}, 120.23).
					Return(errors.New("some error"))
				return m
			}(),
			ginContext:   createInput(`{"tiker":"SBER","target":120.23}`, nil),
			codeResponse: 400,
		},
		{
			name: "API: invalid json on bell set",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				return m
			}(),
			ginContext:   createInput(`qwerty`, nil),
			codeResponse: 400,
		},
		{
			name: "API: wrong input",
			app: func() marketdata.MarketApp {
				m := mock_api.NewMockMarketApp(gomock.NewController(t))
				return m
			}(),
			ginContext:   createWrongInput(nil),
			codeResponse: 400,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			handler := selfapi.NewHandler(test.app)
			handler.HandlerBellsPost(test.ginContext.input)
			require.Equal(t, test.codeResponse, test.ginContext.recoder.Code)
		})
	}
}

func createInput(body string, params map[string]string) ginContext {
	gin.SetMode(gin.TestMode)

	recoder := httptest.NewRecorder()
	input, _ := gin.CreateTestContext(recoder)
	bodyReader := strings.NewReader(body)
	bodyReadCloser := io.NopCloser(bodyReader)
	input.Request = &http.Request{
		Body: bodyReadCloser,
	}
	for key, value := range params {
		input.Params = append(input.Params, gin.Param{
			Key:   key,
			Value: value,
		})
	}
	return ginContext{
		input:   input,
		recoder: recoder,
	}
}

func createWrongInput(params map[string]string) ginContext {
	gin.SetMode(gin.TestMode)

	recoder := httptest.NewRecorder()
	input, _ := gin.CreateTestContext(recoder)
	bodyReader := brokenReader{}
	bodyReadCloser := io.NopCloser(bodyReader)
	input.Request = &http.Request{
		Body: bodyReadCloser,
	}
	for key, value := range params {
		input.Params = append(input.Params, gin.Param{
			Key:   key,
			Value: value,
		})
	}
	return ginContext{
		input:   input,
		recoder: recoder,
	}
}

func (reader brokenReader) Read(b []byte) (n int, err error) {
	return 0, io.ErrUnexpectedEOF
}
