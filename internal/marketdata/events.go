package marketdata

import (
	"errors"
	"fmt"

	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/repository"
)

type MessageTypes int

const (
	Up7 MessageTypes = iota
	Up15
	Up30
	Down7
	Down15
	Down30
	Volume
)

type Messager interface {
	Message() (*Message, error)
}

type EventBigChange struct {
	Time       int64
	EventType  MessageTypes
	Instrument Instrument
}

type EventBigVolume struct {
	Time       int64
	Instrument Instrument
}

type EventBell struct {
	Time       int64
	Price      float64
	Instrument Instrument
}

var ErrMessageError = errors.New("message create error")

// GetNewEvents - return all new events and set them as sended.
func (app marketApp) GetNewEvents() ([]string, error) {
	return app.repo.GetMessages()
}

func (eventBigChange EventBigChange) Message() (*Message, error) {
	messageTemplate := "%s %s на %d%%"
	message := Message{
		Figi:    eventBigChange.Instrument.Figi,
		Time:    eventBigChange.Time,
		MsgType: eventBigChange.EventType,
	}

	level := eventBigChange.Instrument.TriggerLevel
	instrument := eventBigChange.Instrument

	switch eventBigChange.EventType {
	case Down30:
		message.Message = fmt.Sprintf(messageTemplate, instrument, "упал", level*4)
	case Down15:
		message.Message = fmt.Sprintf(messageTemplate, instrument, "упал", level*2)
	case Down7:
		message.Message = fmt.Sprintf(messageTemplate, instrument, "упал", level)
	case Up30:
		message.Message = fmt.Sprintf(messageTemplate, instrument, "вырос", level*4)
	case Up15:
		message.Message = fmt.Sprintf(messageTemplate, instrument, "вырос", level*2)
	case Up7:
		message.Message = fmt.Sprintf(messageTemplate, instrument, "вырос", level)
	default:
		return nil, fmt.Errorf("%w: %s", ErrMessageError, "eventBigChange: unsupported type")
	}

	return &message, nil
}

func (eventBigVolume EventBigVolume) Message() (*Message, error) {
	message := Message{
		Figi:    eventBigVolume.Instrument.Figi,
		Time:    eventBigVolume.Time,
		Message: fmt.Sprintf("Оборот по %s превысил m+3σ", eventBigVolume.Instrument),
		MsgType: Volume,
	}

	return &message, nil
}

func (eventBell EventBell) Message() (*Message, error) {
	message := Message{
		Figi:    eventBell.Instrument.Figi,
		Time:    eventBell.Time,
		Message: fmt.Sprintf("Цена %s достигла %.2f", eventBell.Instrument, eventBell.Price),
	}

	return &message, nil
}

func (eventBigChange *EventBigChange) readDBObject(dbEvent repository.EventBigChange) {
	eventBigChange.EventType = MessageTypes(dbEvent.EventType)
	eventBigChange.Time = dbEvent.Time
	eventBigChange.Instrument.readDBObject(dbEvent.Instrument)
}

func (eventBigVolume *EventBigVolume) readDBObject(dbEvent repository.EventBigVolume) {
	eventBigVolume.Time = dbEvent.Time
	eventBigVolume.Instrument.readDBObject(dbEvent.Instrument)
}

func (eventBell *EventBell) readDBObject(dbEvent repository.EventBell) {
	eventBell.Time = dbEvent.Time
	eventBell.Price = dbEvent.Price
	eventBell.Instrument.readDBObject(dbEvent.Instrument)
}
