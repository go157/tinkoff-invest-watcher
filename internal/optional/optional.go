package optional

type Optional[T any] struct {
	Value T
	Valid bool
}

func From[T any](value T) Optional[T] {
	return Optional[T]{
		Value: value,
		Valid: true,
	}
}
