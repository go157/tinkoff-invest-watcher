package repository

func newEventsBigChangeQuerry() string {
	return `SELECT -- without already written
		ready_to_write.figi,
		ready_to_write.candle_ts,
		ready_to_write.type
	FROM (SELECT -- add message and type
		raw_result.figi as figi,
		raw_result.tiker as tiker,
		raw_result.ts as candle_ts,
		raw_result.tigger_level as tigger_level,
		raw_result.delta as delta,
		case
			when raw_result.delta < -0.04 * raw_result.tigger_level then 5
			when raw_result.delta < -0.02 * raw_result.tigger_level then 4
			when raw_result.delta < -0.01 * raw_result.tigger_level then 3
			when raw_result.delta > 0.04 * raw_result.tigger_level then 2
			when raw_result.delta > 0.02 * raw_result.tigger_level then 1
			when raw_result.delta > 0.01 * raw_result.tigger_level then 0
		else 
			-1
		end as type
	FROM (SELECT -- add delta and tiker
		instruments.id AS figi,
		instruments.tiker AS tiker,
		instruments.tigger_level as tigger_level,
		dates.ts_now AS ts,
		(prices_now.close - COALESCE(prices_before.close,prices_now.open))/COALESCE(prices_before.close,prices_now.open) AS delta
	FROM (SELECT -- get last price and price before timestamp
		last_candle.ts AS ts_now,
		MAX(yesterday.ts) AS ts_before,
		last_candle.id AS figi
	FROM candles AS last_candle
	LEFT JOIN candles AS yesterday
		ON yesterday.ts < last_candle.ts
		AND yesterday.id = last_candle.id
		AND yesterday.ts > $1
	WHERE last_candle.ts > $2
	GROUP BY ts_now, figi) as dates
	INNER JOIN figi AS instruments
		ON dates.figi = instruments.id
	INNER JOIN candles AS prices_now
		ON prices_now.id = dates.figi
		AND prices_now.ts = dates.ts_now
	LEFT JOIN candles AS prices_before
		ON prices_before.id = dates.figi
		AND prices_before.ts = dates.ts_before) AS raw_result
	WHERE raw_result.delta >= 0.01 * raw_result.tigger_level OR raw_result.delta <= -0.01 * raw_result.tigger_level) AS ready_to_write
	LEFT JOIN events as already_happened
		ON ready_to_write.figi = already_happened.figi
		AND ready_to_write.type = already_happened.type
		AND ready_to_write.candle_ts = already_happened.candle_ts
	WHERE already_happened.id isnull`
}

func newEventsBellQuerry() string {
	return `BEGIN;
	CREATE TEMP TABLE triggered_bells ON COMMIT DROP AS
	SELECT -- get triggered prices
		last_candle.figi as figi,
		last_candle.ts as ts,
		bells.value,
		instruments.tiker as tiker,
		bells.from_below,
		price.close*instruments.lot as closeprice
	FROM (SELECT -- last price ts
		id AS figi,
		MAX(ts) AS ts
	FROM candles
	GROUP BY figi) AS last_candle
	INNER JOIN candles as price
	ON last_candle.figi = price.id
	AND last_candle.ts = price.ts
	INNER JOIN bells as bells
	ON bells.figi = last_candle.figi
	INNER JOIN figi as instruments
	ON instruments.id = last_candle.figi
	WHERE
		NOT bells.from_below = (price.close < bells.value);
	
	DELETE FROM bells -- delete triggered bells
	WHERE
		(figi,value) IN (SELECT figi,value FROM triggered_bells);
	
	SELECT
		triggered_bells.figi AS figi,
		triggered_bells.ts AS ts,
		triggered_bells.closeprice AS closeprice
	FROM triggered_bells;
	
	COMMIT;`
}

func newEventsBigVolumeQuerry() string {
	return `SELECT -- without already written
		ready_to_write.figi,
		ready_to_write.candle_ts
	FROM (SELECT -- add volume and tiker
		instruments.id AS figi,
		instruments.tiker AS tiker,
		dates.ts AS candle_ts,
		6 as type
	FROM (SELECT -- get last timestamp
			MAX(ts) AS ts,
			id AS figi
		FROM candles
		GROUP BY figi) as dates
	INNER JOIN figi AS instruments
		ON dates.figi = instruments.id
	INNER JOIN candles AS candle_now
		ON candle_now.id = dates.figi
		AND candle_now.ts = dates.ts
	INNER JOIN figi_aggregates AS aggregates
		ON aggregates.figi = dates.figi
	WHERE
		candle_now.volume > aggregates.volume_avg + 3*aggregates.volume_sigma
		AND instruments.check_volume
		AND aggregates.volume_is_trusted) AS ready_to_write
	LEFT JOIN events as already_happened
		ON ready_to_write.figi = already_happened.figi
		AND ready_to_write.type = already_happened.type
		AND ready_to_write.candle_ts = already_happened.candle_ts
	WHERE already_happened.id ISNULL`
}

func updateAggregatesQuerry() string {
	return `INSERT INTO figi_aggregates SELECT
		with_rmsd.figi AS figi,
		with_rmsd.volume_avg AS volume_avg,
		with_rmsd.rmsd^0.5 AS volume_sigma,
		with_rmsd.volume_is_trusted AS volume_is_trusted	
	FROM (SELECT
			candles.id AS figi,
			average.volume_avg AS volume_avg,
			SUM((average.volume_avg - candles.volume)^2/average.candles_count) AS rmsd,
			average.candles_count > 30 AS volume_is_trusted
		FROM
			candles AS candles
			INNER JOIN (SELECT
				id AS figi,
				SUM(volume)/COUNT(volume) AS volume_avg,
				COUNT(volume) AS candles_count
			FROM
				candles
			GROUP BY
				figi) AS average
			ON average.figi = candles.id
		GROUP BY
			candles.id,
			volume_avg,
			volume_is_trusted) AS with_rmsd
	ON CONFLICT (figi) DO UPDATE
	SET volume_avg = excluded.volume_avg,
		volume_sigma = excluded.volume_sigma,
		volume_is_trusted = excluded.volume_is_trusted;`
}
