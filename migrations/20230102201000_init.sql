-- +goose Up 
-- +goose NO TRANSACTION
CREATE TABLE IF NOT EXISTS figi(
    id TEXT PRIMARY KEY,
    tiker TEXT,
    title TEXT,
    active BOOLEAN,
    min_price INTEGER,
    lot INTEGER,
    currency TEXT,
    tigger_level INTEGER
    );
CREATE TABLE IF NOT EXISTS candles(
    id TEXT,
    ts BIGINT,
    open float8,
    close float8,
    low float8,
    high float8,
    volume INTEGER,
    PRIMARY KEY (id, ts)
    );
CREATE TABLE IF NOT EXISTS events(
    id SERIAL PRIMARY KEY,
    figi TEXT,
    candle_ts BIGINT,
    type INTEGER,
    message TEXT,
    sended BOOLEAN
    );
CREATE TABLE IF NOT EXISTS bells(
    figi TEXT,
    value float8,
    from_below BOOLEAN,
    PRIMARY KEY (figi, value)
    );
CREATE TABLE IF NOT EXISTS figi_aggregates(
    figi TEXT,
    volume_avg INTEGER,
    volume_sigma INTEGER,
    volume_is_trusted BOOLEAN,
    PRIMARY KEY (figi)
    );
ALTER TABLE bells DROP CONSTRAINT IF EXISTS fk_bells_figi;
ALTER TABLE bells ADD CONSTRAINT fk_bells_figi FOREIGN KEY(figi) REFERENCES figi(id);
ALTER TABLE candles DROP CONSTRAINT IF EXISTS fk_candles_figi;
ALTER TABLE candles ADD CONSTRAINT fk_candles_figi FOREIGN KEY(id) REFERENCES figi(id);
ALTER TABLE figi_aggregates DROP CONSTRAINT IF EXISTS fk_aggregates_figi;
ALTER TABLE figi_aggregates ADD CONSTRAINT fk_aggregates_figi FOREIGN KEY(figi) REFERENCES figi(id);
-- +goose Down 
-- +goose NO TRANSACTION
ALTER TABLE bells DROP CONSTRAINT IF EXISTS fk_bells_figi;
ALTER TABLE candles DROP CONSTRAINT IF EXISTS fk_candles_figi;
ALTER TABLE figi_aggregates DROP CONSTRAINT IF EXISTS fk_aggregates_figi;
DROP TABLE figi_aggregates;
DROP TABLE bells;
DROP TABLE events;
DROP TABLE candles;
DROP TABLE figi;
