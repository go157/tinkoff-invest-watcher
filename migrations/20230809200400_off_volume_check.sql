-- +goose Up 
-- +goose NO TRANSACTION
ALTER TABLE figi
  ADD COLUMN check_volume BOOL NOT NULL DEFAULT false;
-- +goose Down 
-- +goose NO TRANSACTION
ALTER TABLE figi
  DROP COLUMN check_volume;
