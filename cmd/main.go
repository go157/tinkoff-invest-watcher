package main

import (
	"log"
	"time"

	"github.com/go-co-op/gocron"
	"github.com/ssummers02/invest-api-go-sdk/pkg"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/dbprovider"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/marketdata"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/repository"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/selfapi"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/settings"
)

func main() {
	// init DB.
	log.SetFlags(log.Lshortfile)
	log.Println("load settings")
	err := settings.ReadSettings()
	if err != nil {
		log.Fatal(err.Error())
	}

	app := InitApp()

	// start shedules.
	loadPriceDelay := 10
	clearDelay := 1000
	sheduler := gocron.NewScheduler(time.Local)
	_, err = sheduler.Every(loadPriceDelay).Minutes().Do(app.LoadPrices)
	if err != nil {
		log.Fatal(err)
	}
	_, err = sheduler.Every(clearDelay).Seconds().Do(app.ClearGarbage)
	if err != nil {
		log.Fatal(err)
	}
	_, err = sheduler.Every(1).Day().At("01:00").Do(app.UpdateAggregates)
	if err != nil {
		log.Fatal(err)
	}
	sheduler.StartAsync()

	// start API.
	handler := selfapi.NewHandler(app)
	selfapi.StartAPIServer(handler)
}

func InitApp() marketdata.MarketApp {
	db, err := dbprovider.NewConn()
	if err != nil {
		log.Fatalln(err)
	}

	repo := repository.NewMarketRepo(db)

	log.Println("init market data")
	pool, err := getServices()
	if err != nil {
		log.Fatalln(err)
	}

	app := marketdata.NewApp(pool, repo, marketdata.DefaultTimer())

	err = app.InitMarketData()
	if err != nil {
		log.Fatalln(err)
	}

	return app
}

func getServices() (*pkg.ServicePool, error) {
	cfg := pkg.Config{
		Token:     settings.AppSettings.Token,
		AccountID: []string{""},
	}

	services, err := pkg.NewServicePool(cfg)
	if err != nil {
		log.Println(err)
	}

	return services, err
}
